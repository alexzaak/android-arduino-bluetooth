package edu.stanford.me202.bluetoothdemo;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Simple UI demonstrating how to connect to a Bluetooth device,
 * send and receive messages using Handlers, and update the UI.
 */
public class BluetoothActivity extends Activity {

    // Tag for logging
    private static final String TAG = "BluetoothActivity";

    // MAC ADDRESS of remote Bluetooth device
    // Replace this with the ADDRESS of your own module
    private static final String ADDRESS = "98:d3:31:fd:4b:f2";

    // The thread that does all the work
    //BluetoothThread bluetoothThread;

    // Handler for writing messages to the Bluetooth connection
    Handler writeHandler;

    private BtConnector btConnector;

    private Button connectBtn;
    private Button disconnectBtn;
    private Button writeBtn;

    private EditText writeFieldEdit;

    private TextView statusTextView;
    private TextView readFieldTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        this.statusTextView = (TextView) findViewById(R.id.statusText);
        this.readFieldTextView = (TextView) findViewById(R.id.readField);

        this.writeFieldEdit = (EditText) findViewById(R.id.writeField);

        this.writeBtn = (Button) findViewById(R.id.writeButton);

        this.writeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeMessage();
            }
        });

        this.disconnectBtn = (Button) findViewById(R.id.disconnectButton);
        this.disconnectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btConnector.stop();
            }
        });

        this.connectBtn = (Button) findViewById(R.id.connectButton);
        this.connectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectService();
            }
        });

        btConnector = new BtConnector(this, mHandler);
        connectService();
    }

    private void writeMessage() {
        if (this.writeFieldEdit.getText() != null) {
            String command = this.writeFieldEdit.getText().toString();
            if (!command.isEmpty()) {
                btConnector.sendMessage(command);
                this.writeFieldEdit.setText("");
            }
        }
    }

    public void connectService() {
        try {
            statusTextView.setText(getString(R.string.connecting));
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (bluetoothAdapter.isEnabled()) {
                btConnector.start();
                btConnector.connectDevice(getString(R.string.device_name));
                Log.d(TAG, "Btservice started - listening");
                statusTextView.setText(R.string.connected);
            } else {
                Log.w(TAG, "Btservice started - bluetooth is not enabled");
                statusTextView.setText(getString(R.string.bt_disabled));
            }
        } catch (Exception e) {
            Log.e(TAG, "Unable to start bt ", e);
            statusTextView.setText(getString(R.string.unable_to_connect) + e);
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BtConnector.MESSAGE_STATE_CHANGE:
                    Log.d(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    break;
                case BtConnector.MESSAGE_WRITE:
                    Log.d(TAG, "MESSAGE_WRITE ");
                    break;
                case BtConnector.MESSAGE_READ:
                    Log.d(TAG, "MESSAGE_READ ");
                    break;
                case BtConnector.MESSAGE_DEVICE_NAME:
                    Log.d(TAG, "MESSAGE_DEVICE_NAME " + msg);
                    break;
                case BtConnector.MESSAGE_TOAST:
                    Log.d(TAG, "MESSAGE_TOAST " + msg);
                    break;
            }
        }
    };


}
